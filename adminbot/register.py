#!/usr/bin/env python
# pylint: disable=C0116
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import os
import logging
from typing import Dict
from db_persistence import DbPersistence
from sqlalchemy import create_engine
from sqlalchemy import MetaData,Table
from sqlalchemy.orm import sessionmaker, scoped_session
from telegram import ReplyKeyboardMarkup, Update, ReplyKeyboardRemove, InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    PicklePersistence,
    CallbackContext,
    CallbackQueryHandler,
    Handler
)

WEBHOOK_URL = str(os.environ.get('WEBHOOK_URL',''))
DB_URI = str(os.environ.get('DATABASE_URL'))
if DB_URI.split(':',1)[0] == 'postgres':
    DB_URI = DB_URI.split(':',1)[0]+'ql:'+ DB_URI.split(':',1)[1]
PORT = int(os.environ.get('PORT',8443))
PLATFORM = str(os.environ.get('PLATFORM'))
TOKEN = str(os.environ.get('TOKEN'))

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

LAB, APPARTMENT_TYPE, BUILDING_NUMBER, HOUSE_NUMBER, PHONE_NUMBER = range(5)

# Shortcut for ConversationHandler.END
END = ConversationHandler.END


def getQuareterTypeKeyboard(keyboardString):
    i = 0
    kb = [] 
    kbRow = []
    for key in keyboardString:        
        if i % 3 == 0:
            if kbRow:
                kb.append(kbRow)
            kbRow = []
        kbRow.append(InlineKeyboardButton(text = key, callback_data=key))
        i+=1  
    if kbRow:
        kb.append(kbRow)

    kb.append([InlineKeyboardButton(text = 'Other', callback_data='Other')])
    return InlineKeyboardMarkup(kb)

def facts_to_str(user_data: Dict[str, str]) -> str:
    print(user_data)
    facts = []

    for key, value in user_data.items():
        facts.append(f'{key} - {value}')

    return "\n".join(facts).join(['\n', '\n'])


def start(update: Update, context: CallbackContext) -> int:
    if update.message.chat.type == 'private':
        update.message.reply_text('Good! Now we can start registering. \n Use the command /register to begin registering. You can send /done to stop registering or if you selected wrong option.  \n\n I am forgetful right now. I will forget you soon...')

def register(update:Update, context: CallbackContext):
    ret = ConversationHandler.END
    if update.message.chat.type != 'private':
        update.message.reply_text('Please send a private message to me so that the group is not spammed.\n\n Click on my icon and select the message icon to start a personal conversation...')
    else:
        context.user_data['FirstName'] = update.message.from_user.first_name
        context.user_data['LastName'] = update.message.from_user.last_name
        # if context.user_data:
        #     update.message.reply_text("You have already registerd. This is what I already know about you"
        #     f"{facts_to_str(context.user_data)}"
        #     "Do you want to change it?", reply_markup=keyboard
        #     )
        # else:
        if not 'quarterTypeList' in context.bot_data:
            context.bot_data['quarterTypeList'] = ['Sh','A','B', 'C', 'D', 'E']
            context.bot_data['Sh_list'] = ['4','4A','5','5A','6']
            context.bot_data['A_list'] = []
            context.bot_data['B_list'] = []
            context.bot_data['C_list'] = []
            context.bot_data['D_list'] = []
            context.bot_data['E_list'] = []
        kb = getQuareterTypeKeyboard(context.bot_data['quarterTypeList'])
        update.message.reply_html("<b>Please select which type of quarter you live in.</b> You can send /done to stop registering or if you selected wrong option", reply_markup=kb)
        ret = APPARTMENT_TYPE
    return ret



def show_data(update: Update, context: CallbackContext) -> None:
    if context.user_data:
        update.message.reply_text(
            f"This is what you already told me: {facts_to_str(context.user_data)} "
        )
    else:
        update.message.reply_text('You need to register first by sending /register \n\n There is also a chance I might have forgotten you. I am still working on my memory..')


def done(update: Update, context: CallbackContext) -> int:
    if 'choice' in context.user_data:
        del context.user_data['choice']

    update.message.reply_text(
        "I learned these facts about you:" f"{facts_to_str(context.user_data)}. You can choose to update information by sending /register anytime.",
        reply_markup=ReplyKeyboardRemove(),
    )
    return ConversationHandler.END

def debPr(update: Update, context: CallbackContext):
    print(update.to_json())
    print(context)
    update.message.reply_text('Enter valid input')

def QuarterSelect(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    
    context.user_data['QuarterType'] = query.data
    print(query.from_user.to_dict())
    kb = getQuareterTypeKeyboard(context.bot_data[context.user_data['QuarterType']+'_list'])
    update.callback_query.edit_message_text(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "Just a few more steps to go \n \n"

        'Select your building \n(You can send /done to stop registering or if you selected wrong option)',reply_markup=kb
    )
    return BUILDING_NUMBER

def houseSelect(update: Update, context: CallbackContext):
    context.user_data['HouseNo'] = update.message.text
    update.message.reply_html(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"

        'Please tell me where you work. You may skip this question by sending /skip if you wish not to tell. (You can send /done to stop registering or if you selected wrong option)')
    return LAB

def WarnWrongPhoneNumer(update: Update, context: CallbackContext):
    update.message.reply_html('You need to click on <b>Send Phone No</b> button appearing in place of you keyboard \n(You can send /done to stop registering or if you selected wrong option)',reply_markup=ReplyKeyboardMarkup([[KeyboardButton('Send Phone No',request_contact=True)]],one_time_keyboard=True))

def contactSelect(update: Update, context: CallbackContext):
    context.user_data['PhNo'] = update.message.contact.phone_number
    update.message.reply_text(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "Thank you for registering!!!",
    )
    return END

def WarnWrongNumber(update: Update, context: CallbackContext):
    update.message.reply_text('Invalid Number!!! Enter a valid number. It can\'t contain special symbols or letters \n(You can send /done to stop registering or if you selected wrong option)')

def WarnWrongString(update: Update, context: CallbackContext):
    update.message.reply_text('Invalid quarter type!!! Enter a valid Quarter type \n(You can send /done to stop registering or if you selected wrong option)')

def queryQuarter(update: Update, context: CallbackContext):
    update.callback_query.edit_message_text('Enter your Quarter Type \n(You can send /done to stop registering or if you selected wrong option)')

def addQuarterType(update: Update, context: CallbackContext):
    context.user_data['QuarterType'] = update.message.text.upper()
    context.user_data['FirstName'] = update.message.from_user.first_name
    context.user_data['LastName'] = update.message.from_user.last_name
    if update.message.text.upper() not in context.bot_data['quarterTypeList']:
        context.bot_data['quarterTypeList'].append(update.message.text)
        context.bot_data[update.message.text+'_list'] = []
    kb = getQuareterTypeKeyboard(context.bot_data[context.user_data['QuarterType']+'_list'])
    update.message.reply_html(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "Just a few more steps to go \n \n"

        '<b>Select your building </b>\n(You can send /done to stop registering or if you selected wrong option)',reply_markup=kb
    )
    return BUILDING_NUMBER

def queryBuilding(update: Update, context: CallbackContext):
    update.callback_query.edit_message_text('Enter your Building Number')

def addBuildingNo(update: Update, context: CallbackContext):
    context.user_data['BuildingNo'] = update.message.text
    if update.message.text not in context.bot_data[context.user_data['QuarterType']+'_list']:
        context.bot_data[context.user_data['QuarterType']+'_list'].append(update.message.text)
    update.message.reply_html(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "Just a few more steps to go \n \n"

        '<b>Enter your Room Number</b>\n(You can send /done to stop registering or if you selected wrong option)'
    )
    return HOUSE_NUMBER

def BuildingSelect(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    context.user_data['BuildingNo'] = query.data
   
    update.callback_query.edit_message_text(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "Just a few more steps to go \n \n"

        'Enter your Room Number \n(You can send /done to stop registering or if you selected wrong option)'
    )
    return HOUSE_NUMBER

def develop(update: Update, context: CallbackContext):
    update.message.reply_markdown('I like it that you want to help in my development. Click on the following link \n \n'
    '[Develop me](https://gitlab.com/lsbharadwaj/adminhelperbot/-/issues)\n\n'
    'Participate by making feature requests, commenting on the open requests, up voting and down voting requests etc... \n \n'
    'Enjoy Developing Me!!!')

def skip(update: Update, context: CallbackContext):
    update.message.reply_html(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "One last step to go. \n \n"

        'Tell me your phone number by clicking the <b>Send Phone No</b> button appearing in place of you keyboard \n(You can send /done to stop registering or if you selected wrong option)',reply_markup=ReplyKeyboardMarkup([[KeyboardButton('Send Phone No',request_contact=True)]],one_time_keyboard=True)
    )   
    return PHONE_NUMBER

def updateLab(update: Update, context: CallbackContext):
    context.user_data['Lab'] = update.message.text.upper()
    update.message.reply_html(
        "Neat! Just so you know, this is what you already told me:"
        f"{facts_to_str(context.user_data)}"
        "One last step to go. \n \n"

        'Tell me your phone number by clicking the <b>Send Phone No</b> button appearing in place of you keyboard \n(You can send /done to stop registering or if you selected wrong option)',reply_markup=ReplyKeyboardMarkup([[KeyboardButton('Send Phone No',request_contact=True)]],one_time_keyboard=True)
    )
    return PHONE_NUMBER

def main() -> None:
    # SQLAlchemy session maker
    def start_session() -> scoped_session:
        engine = create_engine(DB_URI)
        return scoped_session(sessionmaker(bind=engine, autoflush=False))

    # start the session
    session = start_session()
    persistence = DbPersistence(session)

    # Create the Updater and pass it your bot's token.
    updater = Updater(TOKEN,persistence=persistence)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('register', register)],
        states={
            APPARTMENT_TYPE:[CommandHandler('done',done), CallbackQueryHandler(queryQuarter,pattern='^Other$'),CallbackQueryHandler(QuarterSelect),MessageHandler(Filters.regex('^\w{1,2}$'), addQuarterType), MessageHandler(Filters.all, WarnWrongString)],
            BUILDING_NUMBER:[CommandHandler('done',done), CallbackQueryHandler(queryBuilding,pattern='^Other$'),CallbackQueryHandler(BuildingSelect),MessageHandler(Filters.regex('^\d+$'), addBuildingNo), MessageHandler(Filters.all, WarnWrongNumber)],
            HOUSE_NUMBER:[CommandHandler('done',done), MessageHandler(Filters.regex('^\d+$'), houseSelect), MessageHandler(Filters.all, WarnWrongNumber)],
            LAB: [CommandHandler('done',done), CommandHandler('skip',skip), MessageHandler(Filters.all, updateLab)],
            PHONE_NUMBER:[CommandHandler('done',done),MessageHandler(Filters.contact,contactSelect), MessageHandler(Filters.all, WarnWrongPhoneNumer)],
        },
        fallbacks=[MessageHandler(Filters.regex('^Done$'), done)],
        name="my_conversation",
        # persistent=True,
    )

    dispatcher.add_handler(conv_handler)
    show_data_handler = CommandHandler('show_data', show_data)
    dispatcher.add_handler(show_data_handler)
    dispatcher.add_handler(CommandHandler('start',start))
    dispatcher.add_handler(CommandHandler('Develop',develop))

    if PLATFORM == 'PRODUCTION':
        updater.start_webhook(listen="0.0.0.0",port=int(PORT),url_path=TOKEN,webhook_url=WEBHOOK_URL+TOKEN)
    elif PLATFORM =='TESTING':
        updater.start_polling()
    else:
        pass
    # Start the Bot
    
    # 

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    import os
    main()
